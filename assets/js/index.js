var userAgent = window.navigator.userAgent.toLowerCase(),
	inapp = /instagram|fbav|fban|kakao|snapchat|line|micromessenger|twitter/.test(userAgent),
	ios = /iphone|ipod|ipad/.test(userAgent),
	safari = /safari/.test(userAgent),
	android = /android/.test(userAgent),
	chrome = /chrome/.test(userAgent);

if(inapp){
	if(android){
		alert("Please access through Chrome for the best experience")
	} else if(ios){
		alert("Please access through Safari for the best experience")
	}
} else {
	if(ios && !safari) {
		var location = window.location;
		window.open('x-web-search://?ar', "_self");
		window.location.href = location;
	} else if (android) {
		if(!chrome){
			window.location.href = 'googlechrome://navigate?url='+ location.href;
		}
	}
}

function redirectTo(url) {
	window.location.href = url;
}

$(window).on('load', function(){
	//display socmed buttons when marker is clicked
	var burger = $(".burger");
	var displayBurgerClicked = $(".displayBurgerClicked");
	var isBurgerClicked = false;
	
	burger.click(function() {
		if(!isBurgerClicked){
			isBurgerClicked = true;
			displayBurgerClicked.css("display", "grid");
		} else {
			isBurgerClicked = false;
			displayBurgerClicked.hide();
		}
	});
});

AFRAME.registerComponent("foo",{
	init:function() {
		//hide loader when model is loaded
		this.el.addEventListener('model-loaded', e => {
			$(".arjs-loader").hide();
		})

		//action on pinch and pan on the model to rotate and scale accordingly'
		let element = document.querySelector('body');
		let model = document.querySelector('#animated-model');
		let hammertime = new Hammer(element);
		let pinch = new Hammer.Pinch(); // Pinch is not by default in the recognisers
		hammertime.add(pinch); // add it to the Manager instance

		const initial = {
			rotation : model.getAttribute("rotation"),
			scale : model.getAttribute("scale")
		}

		let changedAttr = initial;

		$( "#reset" ).click(function() {
			reset();
		});

		function reset(){
			$.each(initial, function(key1, value1) {
				$.each(value1, function(key2, value2) {
					$("#"+key1+key2).val(value2);
				});
			});
		}

		//set event listener on change/keyup and set up initial
		$.each(initial, function(key1, value1) {
			$.each(value1, function(key2, value2) {
				$("#"+key1+key2).on('change, keyup', function() {
					var currentInput = $(this).val();
					var fixedInput = currentInput.replace(/[A-Za-z!@#$%^&*()]/g, '');
					if(fixedInput != ""){
						$(this).val(fixedInput);
						if(!isNaN(fixedInput)){
							changedAttr[key1][key2] = parseFloat($(this).val());
							console.log(changedAttr)
							model.setAttribute(key1, changedAttr[key1]);
						}
					} else {
						$(this).val(initial[key1][key2]);
					}
				});
			});
		});
		
		$( "#reset" ).click(function() {
			reset();
		});

		hammertime.on('pan', (ev) => {
			let rotation = initial.rotation
			switch(ev.direction) {
				case 2:
					rotation.y = rotation.y - 4
					break;
				case 4:
					rotation.y = rotation.y + 4
					break;
				case 8:
					rotation.x = rotation.x - 4
					break;
				case 16:
					rotation.x = rotation.x + 4
					break;
				default:
					break;
			}
			model.setAttribute("rotation", rotation)
		});

		const initialScale = initial.scale.x;
		const maxScale = initialScale*2; 

	 	hammertime.on("pinch", (ev) => {
			let pinch = ev.scale;
			let scale = model.getAttribute("scale").x*pinch;
			//limit zoom until 2times of initial size and shrink until initial scale
			if(scale <= maxScale && scale >= initialScale){
				let newScale = {x:scale, y:scale, z:scale}
				model.setAttribute("scale", newScale);
			}
		});
	}
});
